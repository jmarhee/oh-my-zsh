
local user='%{$fg[magenta]%}%n@%{$fg[magenta]%}%m%{$reset_color%}'
local pwd='%{$fg[cyan]%}%~%{$reset_color%}'
local rvm=''
if which rvm-prompt &> /dev/null; then
  rvm='%{$fg[green]%}‹$(rvm-prompt i v g)›%{$reset_color%}'
else
  if which rbenv &> /dev/null; then
    rvm='%{$fg[green]%}‹$(rbenv version | sed -e "s/ (set.*$//")›%{$reset_color%}'
  fi
fi

#local avg=`uptime | awk '{print $11}'`
local load=''
#if $(uptime | awk '{print $11}') > 5; then
#	load="%{$fg_bold[red]%}‹$(uptime | awk '{print $11}')›%{$reset_color%}"
#else
#	load="%{$fg[green]%}‹$(uptime | awk '{print $11}')›%{$reset_color%}"
#fi

local return_code='%(?..%{$fg[red]%}%? ↵%{$reset_color%})'
local git_branch='$(git_prompt_status)%{$reset_color%}$(git_prompt_info)%{$reset_color%}'


ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[white]%}["
ZSH_THEME_GIT_PROMPT_SUFFIX="]%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="*"
ZSH_THEME_GIT_PROMPT_CLEAN=""

local username=`whoami`
if [ "$username" = "root" ]; then
	PROMPT="${user} ${pwd} # "
else
	PROMPT="${user} ${pwd} $ "
fi


#RPROMPT="<$(uptime | awk '{print $11}')>"

RPROMPT=$'$(git_prompt_info) $(git_prompt_status)%{$reset_color%} '