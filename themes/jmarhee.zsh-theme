local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"
#PROMPT='%{$fg_bold[blue]%}%M %{$fg_bold[red]%}[%{$fg_bold[green]%}%p %{$fg[cyan]%}%c %{$fg_bold[blue]%}$%{$fg_bold[blue]%} % %{$reset_color%}]:%'
#PROMPT='${ret_status}%{$fg_bold[green]%}%p %{$fg[cyan]%}%c %{$fg_bold[blue]%}$(git_prompt_info)%{$fg_bold[blue]%} % %{$reset_color%}'
#PROMPT='%{$fg_bold[white]%}%M [%{$fg_bold[green]%}%p %{$fg[cyan]%}%c %{$fg_bold[blue]%}% %{$reset_color%} ]'
#PROMPT='%{$fg[white]%}%M %{$fg_bold[red]%}%{$fg[cyan]%}%%p[%{$fg[cyan]%}%c {$fg[cyan]%}%]:% {$reset_color%} %'
ZSH_THEME_GIT_PROMPT_PREFIX="git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$"
